const { checkSchema, validationResult } = require('express-validator');
const ApiError = require('../exceptions/api-error');

module.exports = (schema) => async (req, res, next) => {
  const validations = checkSchema(schema);
  await Promise.all(validations.map(validation => validation.run(req)));
  try {
    const errors = validationResult(req);
    console.log('validation request result:', errors);
    if (!errors.isEmpty()) {
      throw ApiError.BadRequest('Validation error', errors.array());
    }
    next();
  } catch (err) {
    return next(err);
  }
};