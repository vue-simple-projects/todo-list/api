const ApiError = require('../exceptions/api-error');
const tokenService = require('../auth/tokens/token-service');

module.exports = function(req, res, next) {
  try {
    tokenService.tryGetAccessTokenFromRequest(req);
    next(ApiError.BadRequest('User authorized'));
  } catch (err) {
    return next();
  }
};