const ApiError = require('../exceptions/api-error');
const tokenService = require('../auth/tokens/token-service');

module.exports = function(req, res, next) {
  try {
    tokenService.tryGetAccessTokenFromRequest(req);
    next();
  } catch (err) {
    return next(ApiError.UnauthorizedError());
  }
};