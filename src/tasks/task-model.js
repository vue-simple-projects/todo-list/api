const { Schema, model } = require("mongoose");

const TaskSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'User', },
  title: { type: String, default: '', },
  done: { type: Boolean, default: false },
  parent: { type: Schema.Types.ObjectId, ref: 'Task', default: null },
  order: { type: Number, default: -1 },
});

module.exports = model('Task', TaskSchema);
