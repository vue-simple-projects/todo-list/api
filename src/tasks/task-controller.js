const tokenService = require("../auth/tokens/token-service");
const taskService = require("./task-service");
const TaskDto = require("./task-dto");

class TaskController {
  async getAll(req, res, next) {
    try {
      const userData = tokenService.tryGetAccessTokenFromRequest(req);
      const tasks = await taskService.getAll(userData.id);
      const tasksDto = tasks.map((task) => new TaskDto(task));

      return res.status(200).json(tasksDto);
    } catch (err) {
      next(err);
    }
  }
  async create(req, res, next) {
    try {
      const taskPattern = req.body;
      const userData = tokenService.tryGetAccessTokenFromRequest(req);
      taskPattern.userId = userData.id;
      const task = await taskService.create(taskPattern);
      const taskDto = new TaskDto(task);
      return res.status(201).json(taskDto);
    } catch (err) {
      next(err);
    }
  }
  async update(req, res, next) {
    try {
      const taskPattern = req.body;
      taskService.update(taskPattern);
      return res.status(204).json();
    } catch (err) {
      next(err);
    }
  }
  async updateMany(req, res, next) {
    try {
      const tasks = req.body.tasks;
      taskService.updateMany(tasks);
      return res.status(204).json();
    } catch (err) {
      next(err);
    }
  }
  async removeMany(req, res, next) {
    try {
      const tasks = req.body.tasks;
      taskService.removeMany(tasks);
      return res.status(204).json();
    } catch (err) {
      next(err);
    }
  }
  async removeById(req, res, next) {
    try {
      const id = req.params.taskId;
      taskService.remove(id);
      res.status(204).json();
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new TaskController();
