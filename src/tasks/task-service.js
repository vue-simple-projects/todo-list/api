const TaskModel = require("./task-model");

class TaskService {
  async getAll(userId) {
    return (await TaskModel.find({ userId })) || [];
  }
  async getOne(taskId) {
    return await TaskModel.findById(taskId);
  }
  async create(task) {
    const taskData = await TaskModel.create(task);
    return taskData;
  }
  async update(task) {
    const taskData = await TaskModel.findById(task.id);
    for (const key in task) {
      if (key === "id" || !(key in taskData)) {
        continue;
      }
      taskData[key] = task[key];
    }
    taskData.save();
    return taskData;
  }
  updateMany(tasks) {
    const operatios = [];
    tasks.forEach((t) => {
      operatios.push({
        updateOne: {
          filter: { _id: t.id },
          update: {
            $set: {
              title: t.title,
              parent: t.parent,
              order: t.order,
              done: t.done,
            },
          },
        },
      });
    });
    TaskModel.bulkWrite(operatios, { ordered: true, w: 1 });
  }
  async remove(id) {
    const result = await TaskModel.findByIdAndDelete(id);
    return result;
  }
  async removeMany(tasks) {
    const identifiers = tasks.map((t) => t.id);
    const result = await TaskModel.deleteMany({ _id: { $in: identifiers } });
    return result;
  }
}

module.exports = new TaskService();
