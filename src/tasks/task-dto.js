module.exports = class UserDto {
  id;
  title;
  done;
  parent;
  order;

  constructor(model) {
    this.title = model.title;
    this.id = model._id;
    this.parent = model.parent;
    this.order = model.order;
    this.done = model.done;
  }
}