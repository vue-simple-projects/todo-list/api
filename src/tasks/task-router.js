const Router = require("express").Router;
const taskController = require("./task-controller");
const validateAuth = require("../middleware/validate-auth");
const validateRequestParams = require("../middleware/validate-request-params");
const postTaskValidationSchema = require("./validation/post-task-validation-schema");
const putTaskValidationSchema = require("./validation/put-task-validation-schema");
const deleteTaskValidationSchema = require("./validation/delete-task-validation-schema");
const putTasksValidationSchema = require("./validation/put-tasks-validation-schema");
const deleteTasksValidationSchema = require("./validation/delete-tasks-validation-schema");

const router = new Router();

router.get("/", validateAuth, taskController.getAll);
router.post(
  "/",
  validateAuth,
  validateRequestParams(postTaskValidationSchema),
  taskController.create
);
router.put(
  "/",
  validateAuth,
  validateRequestParams(putTasksValidationSchema),
  taskController.updateMany
);
router.delete(
  "/",
  validateAuth,
  validateRequestParams(deleteTasksValidationSchema),
  taskController.removeMany
);
router.put(
  "/:taskId",
  validateAuth,
  validateRequestParams(putTaskValidationSchema),
  taskController.update
);
router.delete(
  "/:taskId",
  validateAuth,
  validateRequestParams(deleteTaskValidationSchema),
  taskController.removeById
);

module.exports = router;
