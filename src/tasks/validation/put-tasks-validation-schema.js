const { isTaskValid, isParentTaskValid } = require("./task-validation");

module.exports = {
  tasks: {
    errorMessage: "tasks must be a non-empty array",
    isArray: {
      bail: true,
      options: {
        min: 0,
      },
    },
  },
  "tasks.*.id": {
    exists: {
      errorMessage: "id is required",
      options: { checkFalsy: true },
    },
    isMongoId: {
      bail: true,
      errorMessage: "Invalid task id",
    },
    isTaskValid: {
      custom: isTaskValid,
    },
  },
  "tasks.*.title": {
    exists: {
      errorMessage: "Title is required",
      options: { checkFalsy: true },
    },
    isString: true,
    isLength: {
      bail: true,
      options: { max: 3000 },
      errorMessage:
        "The title must not be empty and be no more than 3000 characters.",
    },
  },
  "tasks.*.parent": {
    exists: {
      errorMessage: "Parent is required",
      options: { values: "undefined" },
      bail: true,
    },
    isParentTaskValid: {
      custom: isParentTaskValid,
      errorMessage: "Parent task is not exist",
    },
  },
  "tasks.*.order": {
    isNumeric: {
      no_symbols: true,
      errorMessage: "Order must be numeric",
    },
    exists: {
      errorMessage: "Order is required",
      options: { values: "undefined" | "null" },
    },
  },
  "tasks.*.done": {
    exists: {
      errorMessage: "Done is required",
      options: { values: "undefined" | "null" },
    },
    isBoolean: true,
    errorMessage: "Order must be boolean",
  },
};
