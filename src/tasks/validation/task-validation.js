const taskService = require("../task-service");
const tokenService = require("../../auth/tokens/token-service");

async function isTaskValid(taskId, { req }) {
  const task = await taskService.getOne(taskId);
  if (!task) {
    throw new Error(`Task not exist`);
  }

  const user = tokenService.tryGetAccessTokenFromRequest(req);
  if (task.userId.toString() !== user.id) {
    throw new Error(`User does not have access to the task`);
  }
}

async function isParentTaskValid(taskId, { req }) {
  if (taskId === null) {
    return;
  }
  const task = await taskService.getOne(taskId);
  if (!task) {
    throw new Error(`Task not exist`);
  }
}

module.exports = {
  isTaskValid,
  isParentTaskValid,
};
