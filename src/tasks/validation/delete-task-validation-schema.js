const { isTaskValid } = require("./task-validation");

module.exports = {
  taskId: {
    exists: {
      errorMessage: "id is required",
      options: { checkFalsy: true },
    },
    isMongoId: {
      errorMessage: "Invalid task id",
      bail: true,
    },
    isTaskValid: {
      custom: isTaskValid,
    },
  },
};
