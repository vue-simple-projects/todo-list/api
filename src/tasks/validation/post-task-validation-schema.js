const { isTaskValid, isParentTaskValid } = require("./task-validation");

module.exports = {
  title: {
    exists: {
      errorMessage: "Title is required",
    },
    isString: true,
    isLength: {
      bail: true,
      options: { max: 3000 },
      errorMessage: "The title must be no more than 3000 characters.",
    },
  },
  parent: {
    optional: true,
    exists: {
      errorMessage: "Invalid parent id",
      options: { values: "undefined" },
      bail: true,
    },
    isParentTaskValid: {
      custom: isParentTaskValid,
      errorMessage: "Parent task is not exist",
    },
  },
  order: {
    optional: true,
    isNumeric: {
      no_symbols: true,
      errorMessage: "Order must be numeric",
    },
  },
  done: {
    optional: true,
    isBoolean: true,
    errorMessage: "Order must be boolean",
  },
};
