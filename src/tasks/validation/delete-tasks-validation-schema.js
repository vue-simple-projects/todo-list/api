const { isTaskValid } = require("./task-validation");

module.exports = {
  tasks: {
    errorMessage: "tasks must be a non-empty array",
    isArray: {
      bail: true,
      options: {
        min: 0,
      },
    },
  },
  "tasks.*.id": {
    exists: {
      errorMessage: "id is required",
      options: { checkFalsy: true },
    },
    isMongoId: {
      bail: true,
      errorMessage: "Invalid task id",
    },
    isTaskValid: {
      custom: isTaskValid,
    },
  },
};
