const Router = require('express').Router;
const userController = require('./user-controller');
const activationRouter = require('./activation/activation-router');
const validateAuth = require('../middleware/validate-auth');

const router = new Router();
router.use(activationRouter)

router.get(
  '/',
   validateAuth,
  userController.getAllUsers
);
router.get(
  '/by-token',
   validateAuth,
  userController.getByToken
);

module.exports = router;
