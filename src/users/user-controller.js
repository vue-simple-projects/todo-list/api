const userService = require('./user-service');
const tokenService = require('../auth/tokens/token-service');
const UserDto = require('./dto/user-dto');

class UserController {
  async getAllUsers(req, res, next) {
    try {
      const users = await userService.getAllUsers();
      return res.status(200).json({ users });
    } catch (err) {
      next(err);
    }
  }
  async getByToken(req, res, next) {
    try {
      const user = tokenService.tryGetAccessTokenFromRequest(req);
      const dto = new UserDto(user);
      return res.status(200).json(dto);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new UserController();
