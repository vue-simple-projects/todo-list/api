const UserModel = require('./user-model');
const bcrypt = require('bcrypt');
const UserDto = require('./dto/user-dto');
const tokenService = require('../auth/tokens/token-service');

class UserService {
  async signUp(email, password) {
    const hashPassword = await bcrypt.hash(password, 5);
    const user = await UserModel.create({ email, password: hashPassword });
    const userDto = new UserDto(user);
    return userDto;
  }

  async removeByEmail(email) {
    const result = await UserModel.deleteOne({ email });
    return !!result.deletedCount;
  }

  async getDtoByEmail(email) {
    const user = await UserModel.findOne({ email });
    return user ? new UserDto(user) : null;
  }

  async getByEmail(email) {
    return await UserModel.findOne({ email });
  }

  async activate(id) {
    const user = await this.getById(id);
    user.activated = true;
    await user.save();
    return new UserDto(user);
  }

  async getById(id) {
    return await UserModel.findById(id);
  }

  async getByRefreshToken(refreshToken) {
    const userData = tokenService.tryDecryptRefreshToken(refreshToken);
    const user = await this.getById(userData.id);
    return new UserDto(user);
  }

  async getAllUsers(refreshToken) {
    const users = await UserModel.find();
    return users.map(u => new UserDto(u));
  }
}

module.exports = new UserService();
