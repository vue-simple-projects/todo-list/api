const userService = require('../../user-service');
const activationService = require('../activation-service').default;

async function checkActivationLinkIsExist(activationLink) {
  const model = await activationService.getByLink(activationLink);
  if (!model) {
    throw new Error('Invalid activation link');
  }

  const user = await userService.getById(model.userId);
  if (!user) {
    throw new Error('Activation link is not associated with any user');
  }
}

module.exports = {
  link: {
    isUUID: {
      options: 4,
      bail: true,
    },
    isActivationLinkExist: {
      custom: checkActivationLinkIsExist,
    },
  },
};
