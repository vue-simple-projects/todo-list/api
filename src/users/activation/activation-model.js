const { Schema, model } = require('mongoose');

const UserActivationSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'User' },
    link: { type: String, required: true, unique: true },
});

module.exports = model(
    'UserActivation',
    UserActivationSchema,
);
