const uuid = require('uuid');
const ActivationModel = require('./activation-model');

class ActivationService {
  async createByUserId(userId) {
    const link = uuid.v4();
    const activation = await ActivationModel.create({ userId, link });
    return `${process.env.API_URL}/api/activate/${activation.link}`;
  }

  async getByLink(link) {
    return await ActivationModel.findOne({ link });
  }

  async removeByLink(link) {
    const result = await ActivationModel.deleteOne({ link });
    return !!result.deletedCount;
  }
}

module.exports = new ActivationService();
