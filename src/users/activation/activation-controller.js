const userService = require('../user-service');
const activationService = require('./activation-service');

class ActivationController {
  async activate(req, res, next) {
    try {
      const link = req.params.link;
      const activation = await activationService.getByLink(link);

      const user = await userService.activate(activation.userId);
      await activationService.removeByLink(link);

      return res.status(200).json({ user });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new ActivationController();