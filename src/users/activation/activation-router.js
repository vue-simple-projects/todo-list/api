const Router = require('express').Router;
const activationController = require('./activation-controller');
const activationValidationSchema = require('./validation/activation-validation-schema');
const validateRequestParams = require('../../middleware/validate-request-params');

const router = new Router();

router.post(
  '/activation',
  validateRequestParams(activationValidationSchema),
  activationController.activate,
);

module.exports = router;