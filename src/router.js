const Router = require('express').Router;
const usersRouter = require('./users/user-router');
const tasksRouter = require('./tasks/task-router');
const authRouter = require('./auth/auth-router');

const router = new Router();
router.use('/users', usersRouter);
router.use('/tasks', tasksRouter);
router.use('/auth', authRouter);

module.exports = router;