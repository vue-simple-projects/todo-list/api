module.exports = class ApiError extends Error {
  status;
  errors;

  constructor(status, message, errors) {
    super(message);
    this.status = status;
    this.errors = errors;
  }

  static UnauthorizedError() {
    return new ApiError(401, 'User not authorized');
  }

  static ForbiddenError(message = 'User does not have access') {
    return new ApiError(403, message);
  }

  static BadRequest(message, errors) {
    return new ApiError(400, message, errors);
  }

  static InternalServerError(message) {
    return new ApiError(500, message);
  }
}