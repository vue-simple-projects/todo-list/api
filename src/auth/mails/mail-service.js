const nodemailer = require('nodemailer');

class MailService {
  constructor() {
    this.email = process.env.SMTP_USER;
    this.apiUrl = process.env.CLIENT_URL;
    this.transport = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: false,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS,
      },
    });
  }

  async sendActivationMail(email, activationLink) {
    // await this.transport.sendMail({
    //   from: this.email,
    //   to: email,
    //   subject: `Активация аккаунта на ${this.apiUrl}`,
    //   text: '',
    //   html: `
    //     <div>
    //       <h1>Для активации перейдите по ссылке</h1>
    //       <a href="${activationLink}">${activationLink}</a>
    //     </div>
    //   `,
    // });
  }
}

module.exports = new MailService();
