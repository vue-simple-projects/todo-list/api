const ApiError = require("../../exceptions/api-error");
const tokenService = require("../tokens/token-service");

module.exports = async (req, res, next) => {
  try {
    const refreshToken = req.cookies?.refreshToken;
    if (!refreshToken) {
      throw ApiError.UnauthorizedError();
    }

    const decryptedToken = tokenService.tryDecryptRefreshToken(refreshToken);
    const savedToken = await tokenService.getToken(refreshToken);
    if (!decryptedToken || !savedToken) {
      throw ApiError.UnauthorizedError();
    }

    next();
  } catch (e) {
    return next(e);
  }
};
