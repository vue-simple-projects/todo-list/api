const userService = require('../../users/user-service');

async function checkEmailNotInUse(email) {
  const user = await userService.getDtoByEmail(email);
  if (user) {
    throw new Error('Email already in use');
  }
}

module.exports = {
  email: {
    exists: {
      errorMessage: 'Email is required',
      options: { checkFalsy: true },
    },
    isEmail: {
      bail: true,
      errorMessage: 'Please provide valid email',
    },
    normalizeEmail: true,
    emailNotInUse: {
      custom: checkEmailNotInUse,
    },
  },
  password: {
    exists: { errorMessage: 'Password is required' },
    isString: { errorMessage: 'password should be string' },
    isLength: {
      options: { min: 8, max: 32 },
      errorMessage:
        'The password must be no less than 8 and no more than 32 characters.',
    },
    // isStrongPassword: {
    //   options: {
    //     minLength: 8,
    //     minLowercase: 1,
    //     minUppercase: 1,
    //     minNumbers: 1,
    //   },
    //   errorMessage: "Password must be greater than 8, no more than 32 and contain at least one uppercase letter, one lowercase letter, and one number",
    // },
  },
};
