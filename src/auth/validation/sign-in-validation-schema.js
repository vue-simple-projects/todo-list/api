const userService = require("../../users/user-service");
const bcrypt = require("bcrypt");

async function checkEmailInUse(email) {
  const user = await userService.getDtoByEmail(email);
  if (!user) {
    throw new Error("Email is not registered");
  }
}

async function checkPassword(password, { req }) {
  const email = req.body.email;
  const user = await userService.getByEmail(email);
  if (!user) {
    return;
  }

  const isPasswordsEquals = await bcrypt.compare(password, user.password);
  if (!isPasswordsEquals) {
    throw new Error("Invalid password");
  }
}

module.exports = {
  email: {
    exists: {
      errorMessage: "Email is required",
      options: { checkFalsy: true },
    },
    isEmail: {
      bail: true,
      errorMessage: "Please provide valid email",
    },
    normalizeEmail: true,
    isEmailInUse: {
      custom: checkEmailInUse,
      errorMessage: "Email is not registered",
    },
  },
  password: {
    exists: { errorMessage: "Password is required" },
    isString: {
      bail: true,
      errorMessage: "password should be string",
    },
    isPasswordValid: {
      custom: checkPassword,
      errorMessage: "Invalid password",
    },
  },
};
