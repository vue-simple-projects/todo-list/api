const Router = require("express").Router;
const authController = require("./auth-controller");
const signUpValidationSchema = require("./validation/sign-up-validation-schema");
const signInValidationSchema = require("./validation/sign-in-validation-schema");
const validateRefreshToken = require("./validation/validate-refresh-token");
const validateAuth = require("../middleware/validate-auth");
const validateNoAuth = require("../middleware/validate-no-auth");
const validateRequestParams = require("../middleware/validate-request-params");

const router = new Router();

router.post(
  "/",
  validateNoAuth,
  validateRequestParams(signUpValidationSchema),
  authController.signUp
);
router.patch(
  "/",
  validateRequestParams(signInValidationSchema),
  authController.signIn
);
router.delete("/", validateAuth, authController.signOut);
router.get("/", validateRefreshToken, authController.refresh);

module.exports = router;
