const jwt = require("jsonwebtoken");
const tokenModel = require("./token-model");

class TokenService {
  async setupTokenToUser(user) {
    const tokens = this.generateTokens(user);
    await this.saveRefreshToken(user.id, tokens.refreshToken);
    return tokens;
  }

  generateTokens(payload) {
    const p = { ...payload };
    const accessToken = jwt.sign(p, process.env.JWT_ACCESS_SECRET, {
      expiresIn: process.env.JWT_ACCESS_EXPIRES_IN,
    });

    const refreshToken = jwt.sign(p, process.env.JWT_REFRESH_SECRET, {
      expiresIn: process.env.JWT_REFRESH_EXPIRES_IN,
    });

    return {
      accessToken,
      refreshToken,
    };
  }

  async saveRefreshToken(userId, refreshToken) {
    // TODO: multi-device situation is not taken into account. Answer: fingerprintjs + user-aget
    // todo: use redis for saving token
    const tokenData = await tokenModel.findOne({ userId });
    if (tokenData) {
      tokenData.refreshToken = refreshToken;
      return tokenData.save();
    }
    const token = await tokenModel.create({ userId, refreshToken });
    return token;
  }

  async removeToken(refreshToken) {
    const token = await tokenModel.deleteOne({ refreshToken });
    return token;
  }

  async getToken(refreshToken) {
    const token = await tokenModel.findOne({ refreshToken });
    return token;
  }

  tryDecryptAccessToken(token) {
    return this.tryDecryptToken(token, process.env.JWT_ACCESS_SECRET);
  }

  tryDecryptRefreshToken(token) {
    return this.tryDecryptToken(token, process.env.JWT_REFRESH_SECRET);
  }

  tryDecryptToken(token, secret) {
    try {
      const data = jwt.verify(token, secret);
      return data;
    } catch (e) {
      return null;
    }
  }

  tryGetAccessTokenFromRequest(req) {
    const authorizationHeader = req.headers.authorization;
    if (!authorizationHeader) {
      throw new Error("Authorization header is empty");
    }

    const accessToken = authorizationHeader.split(" ")[1];
    if (!accessToken) {
      throw new Error("Invalid authorization header");
    }

    const userData = this.tryDecryptAccessToken(accessToken);
    if (!userData) {
      throw new Error("The access token could not be decrypted");
    }

    return userData;
  }
}

module.exports = new TokenService();
