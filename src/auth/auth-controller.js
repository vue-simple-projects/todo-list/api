const tokenService = require("./tokens/token-service");
const userService = require("../users/user-service");
const activationService = require("../users/activation/activation-service");
const mailService = require("./mails/mail-service");

class AuthController {
  static COOKIE_PARAMS = {
    maxAge: process.env.JWT_REFRESH_EXPIRES_IN,
    httpOnly: true,
    path: "/api/auth",
  };

  async signUp(req, res, next) {
    try {
      const { email, password } = req.body;
      const userDto = await userService.signUp(email, password);

      //  create a link for activation and send it to
      const link = await activationService.createByUserId(userDto.id);
      await mailService.sendActivationMail(email, link);

      const tokens = await tokenService.setupTokenToUser(userDto);
      res.cookie(
        "refreshToken",
        tokens.refreshToken,
        AuthController.COOKIE_PARAMS
      );

      return res.status(201).json({
        user: userDto,
        token: tokens.accessToken,
      });
    } catch (err) {
      await userService.removeByEmail(email);
      next(err);
    }
  }
  async signIn(req, res, next) {
    try {
      const { email } = req.body;
      const user = await userService.getDtoByEmail(email);
      const tokens = await tokenService.setupTokenToUser(user);
      res.cookie(
        "refreshToken",
        tokens.refreshToken,
        AuthController.COOKIE_PARAMS
      );

      return res.status(200).json({
        user,
        token: tokens.accessToken,
      });
    } catch (err) {
      next(err);
    }
  }

  async signOut(req, res, next) {
    try {
      const refreshToken = req.cookies?.refreshToken;
      await tokenService.removeToken(refreshToken);
      res.clearCookie("refreshToken");
      return res.status(204).json();
    } catch (err) {
      next(err);
    }
  }

  async refresh(req, res, next) {
    try {
      const refreshToken = req.cookies?.refreshToken;
      const user = await userService.getByRefreshToken(refreshToken);
      await tokenService.removeToken(refreshToken);
      const tokens = await tokenService.setupTokenToUser(user);
      res.cookie(
        "refreshToken",
        tokens.refreshToken,
        AuthController.COOKIE_PARAMS
      );

      return res
        .header("Cache-Control", "no-store, no-cache, must-revalidate")
        .header("Pragma", "no-cache")
        .status(200)
        .json({
          user,
          token: tokens.accessToken,
        });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new AuthController();
