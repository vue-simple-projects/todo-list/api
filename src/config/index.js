const dbConfig = require('./db.config');
const corsConfig = require('./cors.config');

module.exports = {
  dbConfig,
  corsConfig,
}