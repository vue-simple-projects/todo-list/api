require('dotenv').config();
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const router = require('./router');
const errorMiddleware = require('./middleware/error-middleware');
const { dbConfig, corsConfig } = require('./config');
const PORT = process.env.API_PORT || 5000;

const app = express();
app.use(express.json());
app.use(cors(corsConfig));
app.use(cookieParser());
app.use('/api', router);
app.use(errorMiddleware);

const start = async () => {
  try {
    await mongoose.connect(dbConfig.url, dbConfig.options);
    console.log('Mongoose connection state ' + mongoose.connection.readyState);
    app.listen(PORT, () => console.log('Server is started on port ' + PORT));
  } catch (err) {
    console.error(err);
  }
};

start();
