# ToDo list backend

## Key technologies

- Node
- Express
- MongoDB

## Functionality

- Registration by mail/password
- Confirmation by mail
- Create, edit, delete tasks

## Setup

- npm install
- npm run dev

### .env example

```env
PORT=5000
CLIENT_URL=http://localhost:3000

DB_USER=root,
DB_PASSWORD=example,
DB_HOST=localhost,
DB_PORT=27017,
DB_NAME=todoDb,

JWT_ACCESS_SECRET=some-random-string
JWT_ACCESS_EXPIRES_IN=30m
JWT_REFRESH_SECRET=another-random-string
JWT_REFRESH_EXPIRES_IN=2592000000

SMTP_HOST=smtp.company.com
SMTP_PORT=587
SMTP_USER=noreply@company.com
SMTP_PASS=password
```

### Dockerfile example

```dockerfile
FROM node:alpine

WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
CMD npm run prod
```
